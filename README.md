# IFI-Covid19
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is simple for covid patient management.
	
## Technologies
Project is created with:
* Springboot version: 2.5.6
* react version: 17.0.2
* mysql version: 5.7
	
## Setup
To run this project, follow three steps:

* Clone source from gitlab 
```
git clone https://gitlab.com/quannmUET/ifi-covid19.git
```
* make a new user for database
```
CREATE USER 'covid19'@'localhost' IDENTIFIED BY '123456789';
```
* add libraries for front end
```
npm install
```

