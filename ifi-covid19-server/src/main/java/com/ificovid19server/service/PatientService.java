package com.ificovid19server.service;


import com.ificovid19server.models.Patient;

import java.util.List;

public interface PatientService {
    Patient findById(Long id);

    List<Patient> findAll();

    List<Patient> findAllActive();

    Patient deletePatient(Long id);

    List<Patient> searchWithKeyWord(String name);

    List<Patient> searchWithStatus(int valueOf);

    List<Patient> searchWithStatusAndKeyWord(String name, int valueOf);

    Patient save(Patient patient);
}
