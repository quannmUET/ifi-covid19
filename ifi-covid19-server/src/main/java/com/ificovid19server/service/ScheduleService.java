package com.ificovid19server.service;

import com.ificovid19server.models.Schedule;

public interface ScheduleService {
    Schedule save (Schedule schedule);

    Schedule findById(Long id);
}
