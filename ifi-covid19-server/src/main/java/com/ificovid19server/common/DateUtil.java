package com.ificovid19server.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

public class DateUtil {
    private static String formatter = "dd/MM/yyyy";
    private static String formatter1 = "dd-MM-yyyy";
    private static String formatter2 = "dd/MM/yyyy hh:mm:ss";

    public static String DateToString(Date date) {
        return new SimpleDateFormat(formatter).format(date);
    }

    public static Date StringToDate(String strDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat(formatter).parse(strDate);

        } catch (Exception e) {
        }
        return date;
    }

    public static String DateToStringDetail(Date date) {
        return new SimpleDateFormat(formatter2).format(date);
    }

    public static Date StringDetailToDate(String strDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat(formatter2).parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static int calculateAge(Date birthDate) {
        Calendar a = getCalendar(birthDate);
        Calendar b = getCalendar(new Date());
        int diff = b.get(YEAR) - a.get(YEAR);
        if (a.get(MONTH) > b.get(MONTH) ||
                (a.get(MONTH) == b.get(MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }
    public static String CalendarToString(Calendar c){
        Date date = c.getTime();
        return new SimpleDateFormat(formatter).format(date);
    }
}
