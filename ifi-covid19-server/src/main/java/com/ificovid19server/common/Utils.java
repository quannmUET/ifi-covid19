package com.ificovid19server.common;

public class Utils {
    public static String getCurrentStatus(int status) {
        String strStatus = "";
        if (status == 0) {
            strStatus = Constant.JUST_DETECTED;
        } else if (status == 1) {
            strStatus = Constant.TREATING;
        } else if (status == 2) {
            strStatus = Constant.DIED;
        } else if (status == 3) {
            strStatus = Constant.RECOVERED;
        }
        return strStatus;
    }
}
