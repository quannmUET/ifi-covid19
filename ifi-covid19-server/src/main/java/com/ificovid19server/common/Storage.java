package com.ificovid19server.common;

import com.ificovid19server.models.Patient;
import com.ificovid19server.models.Person;
import com.ificovid19server.models.Schedule;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Storage {
    private static String path = Constant.IMAGE_PATH;
    private static Logger log = LoggerFactory.getLogger(Storage.class);
//    private static String excelFilePath = Constant.REPORT_PATH + new Date().getTime()+new Date().getSeconds() + "_listPatient.xlsx";

    public  Map<String, String> storeFile(String image, String identify ) {
        Map<String, String> map = new HashMap<>();
        try {
//            byte[] imageByte = Base64.getDecoder().decode(image);
            //lay dir
            //create unique name
            String fileName = (new Date()).getTime() + "_" + identify  + ".txt";
            //write image to file local

            FileOutputStream outputStream = new FileOutputStream(path+"/"+fileName);
            byte[] strToBytes = image.getBytes();
            outputStream.write(strToBytes);
            outputStream.close();

//            OutputStream stream = new FileOutputStream(path + "/" + fileName);
//            stream.write(imageByte);
//            stream.write(imageByte);

            Path paths = Paths.get(path);
            Path targetLocation = paths.resolve(fileName);
            File fileServerFile = targetLocation.toFile();
            if (fileServerFile.exists()) {
                map.put(fileName, targetLocation.toString());
                return map;
            }

        } catch (Exception ex) {
            ex.getMessage();
        }
        return map;
    }

    public static String getImage (String fileName){
        String image = null;
        try{
            BufferedReader reader = new BufferedReader(new FileReader(path+fileName));
            image = reader.readLine();
            reader.close();
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return image;
    }

    public String exportWord(Patient patient) {
        log.info("Doing Export to word docx....");
        XWPFDocument doc = new XWPFDocument();
        XWPFParagraph xws = doc.createParagraph();
        XWPFRun r1 = xws.createRun();
        r1.setBold(true);
        r1.setFontSize(20);
        r1.setTextPosition(2);
        r1.setText("Patient Information");
        Map<String, String> map = new HashMap<>();

        // Add giá Key-Value vào HashMap
        map.put("ID: ", String.valueOf(patient.getId()));
        map.put("Name: ", patient.getName());
        map.put("Gender: ", patient.getGender());
        map.put("Birth date: ", DateUtil.DateToString(patient.getDateOfBirth()));
        map.put("Age: ", String.valueOf(DateUtil.calculateAge(patient.getDateOfBirth())));
        map.put("Address: ", patient.getAddress().getDetail());
        map.put("Detection time: ", DateUtil.DateToStringDetail(patient.getDetectionDate()));
        map.put("Heath current status: ", Utils.getCurrentStatus(patient.getStatus()));
        map.put("Hospital: ", patient.getHospital().getName());
        map.put("Hospital Address: ", patient.getHospital().getAddress().getDetail()+", "+patient.getHospital().getAddress().getWard()+", "+patient.getHospital().getAddress().getDistrict()+", "+patient.getHospital().getAddress().getProvince());

        map.forEach((key, value) -> {
            XWPFParagraph xwpfParagraph = doc.createParagraph();
            XWPFRun run = xwpfParagraph.createRun();
            run.setText(key + value);
        });
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        XWPFRun run = xwpfParagraph.createRun();
        run.setBold(true);
        run.setText("Schedule move and contact: ");
        List<Schedule> schedules = patient.getScheduleList();
        for (Schedule sc : schedules) {
            XWPFParagraph xw = doc.createParagraph();
            XWPFRun r = xw.createRun();
            r.setText(sc.getAddress().getDetail() +", "+sc.getAddress().getWard()+ ", "+sc.getAddress().getDistrict()+", "+sc.getAddress().getProvince()+ " - " + DateUtil.DateToStringDetail(sc.getDate()));
            int i = 1;
            List<Person> people =sc.getPeople();
            for (Person p : people) {
                XWPFParagraph x2 = doc.createParagraph();
                XWPFRun r2 = x2.createRun();
                r2.setText(i + ".Meeting " + p.getName() + " " + DateUtil.calculateAge(p.getDateOfBirth()) + " years old " + " have phone number : " + p.getPhone());
                ++i;
            }

        }
        String fileName = new Date().getTime() + "_" + patient.getId() + "_Info.docx";
        try (FileOutputStream out = new FileOutputStream(Constant.REPORT_PATH + fileName)) {
            doc.write(out);
            doc.close();
            log.info("Wrote to file: " + Constant.REPORT_PATH + fileName);
//            CommonUtil.openFile(new File(Constant.REPORT_PATH+fileName));
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return fileName;

    }

    public void exportExcel(List<Patient> patients) {
        List<Patient> listNew = new ArrayList();
        if (patients.size() > 10) {
            //in 10 bệnh nhân
            for (int i = 0; i < 10; i++) {
                listNew.add(patients.get(i));
            }
        } else {
            listNew = patients;
        }
        writeExcel(listNew);
    }

    public void writeExcel(List<Patient> patients) {
        // Create Workbook
        Workbook workbook = getWorkbook();

        // Create sheet
        Sheet sheet = workbook.createSheet("List patient covid 19"); // Create sheet with sheet name

        int rowIndex = 0;

        // Write header
        writeHeader(sheet, rowIndex);

        // Write data
        rowIndex++;
        for (Patient patient : patients) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeResult(patient, row);
            rowIndex++;
        }


        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook);
    }

    public Workbook getWorkbook() {
        Workbook workbook = null;
        try {
//            if (excelFilePath.endsWith("xlsx")) {
                workbook = new XSSFWorkbook();
//            } else if (excelFilePath.endsWith("xls")) {
//                workbook = new HSSFWorkbook();
//            } else {
//                throw new IllegalArgumentException("The specified file is not Excel file");
//            }
        } catch(Exception e)
        {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return workbook;
    }

    public void writeHeader(Sheet sheet, int rowIndex) {
// create CellStyle
        CellStyle cellStyle = createStyleForHeader(sheet);

        // Create row
        Row row = sheet.createRow(rowIndex);

        // Create cells
        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Patient ID");

        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Patient name");

        cell = row.createCell(2);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Patient Age");

        cell = row.createCell(3);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Patient gender");

        cell = row.createCell(4);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Detection Time");

        cell = row.createCell(5);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Patient current status");
    }

    public void writeResult(Patient patient, Row row) {
        Cell cell = row.createCell(0);
        cell.setCellValue(patient.getIdentityCard());

        cell = row.createCell(1);
        cell.setCellValue(patient.getName());

        cell = row.createCell(2);
        cell.setCellValue(DateUtil.calculateAge(patient.getDateOfBirth()));

        cell = row.createCell(3);
        cell.setCellValue(patient.getGender());

        cell = row.createCell(4);
        cell.setCellValue(DateUtil.DateToStringDetail(patient.getDetectionDate()));

        cell = row.createCell(5);
        cell.setCellValue(Utils.getCurrentStatus(patient.getStatus()));

    }

    public CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }

    public void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    public void createOutputFile(Workbook workbook) {
        String fileName = new Date().getTime() + "_" + "_listPatient.xlsx";
        try (FileOutputStream out = new FileOutputStream(Constant.REPORT_PATH+fileName)) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
