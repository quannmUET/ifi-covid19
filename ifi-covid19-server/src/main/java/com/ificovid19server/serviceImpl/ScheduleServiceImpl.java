package com.ificovid19server.serviceImpl;

import com.ificovid19server.models.Schedule;
import com.ificovid19server.repos.ScheduleRepo;
import com.ificovid19server.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepo scheduleRepo;

    @Override
    public Schedule save(Schedule schedule) {
        return scheduleRepo.save(schedule);
    }

    @Override
    public Schedule findById(Long id) {
        return scheduleRepo.findById(id).orElse(null);
    }
}
