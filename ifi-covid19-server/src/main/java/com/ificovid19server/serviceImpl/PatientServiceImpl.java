package com.ificovid19server.serviceImpl;

import com.ificovid19server.common.Storage;
import com.ificovid19server.models.Patient;
import com.ificovid19server.repos.PatientRepository;
import com.ificovid19server.service.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private static boolean STATUS_DELETED = true;
    private static long STATUS_JUST = 0L;
    private static long STATUS_TREATING = 1L;
    private static long STATUS_DIED = 2L;
    private static long STATUS_CURED = 3L;

    @Autowired
    private PatientRepository patientRepository;
    private static Logger log = LoggerFactory.getLogger(Storage.class);

    @Override
    public Patient findById(Long id) {
        try {
            Patient patient = patientRepository.findById(id).orElse(null);
            return patient;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Patient> findAll() {
        return patientRepository.findAll();
    }

    @Override
    public List<Patient> findAllActive() {
        return patientRepository.findAllActive();
    }

    @Override
    public Patient deletePatient(Long id) {
        Patient patient = patientRepository.findById(id).orElse(null);
        patient.setDeleted(STATUS_DELETED);
        return patientRepository.save(patient);
    }

    @Override
    public List<Patient> searchWithStatusAndKeyWord(String kw, int status) {
        return patientRepository.findAllByStatusAndKeyword(status, kw);
    }

    @Override
    public Patient save(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public List<Patient> searchWithKeyWord(String kw) {
        return patientRepository.findAllByKeyword(kw);
    }

    @Override
    public List<Patient> searchWithStatus(int status) {
        return patientRepository.findAllByStatus(status);
    }

}
