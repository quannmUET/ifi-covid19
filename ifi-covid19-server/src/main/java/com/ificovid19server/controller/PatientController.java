package com.ificovid19server.controller;

import com.ificovid19server.DTO.ScheduleDTO;
import com.ificovid19server.common.Constant;
import com.ificovid19server.common.Storage;
import com.ificovid19server.common.Valid;
import com.ificovid19server.models.*;
import com.ificovid19server.repos.HospitalRepo;
import com.ificovid19server.service.PatientService;
import com.ificovid19server.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PatientController {
    @Autowired
    private PatientService patientService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private HospitalRepo hospitalRepo;

    @GetMapping("/hello")
    public String helloWorld() {
        return "Hello world";
    }

    @GetMapping("/patient/{id}")
    public Patient findById(@PathVariable("id") Long id) {
        Patient patient = patientService.findById(id);
        if (patient != null && patient.getImage() != null) {
            //get base64 image by name
            String image = Storage.getImage(patient.getImage());
            patient.setImage(image);
        }
        return patient;
    }

    @GetMapping("/patient/all")
    public List<Patient> getAll() {
        List<Patient> patientList = patientService.findAllActive();
        for (Patient patient : patientList) {
            String image = Storage.getImage(patient.getImage());
            patient.setImage(image);
        }
        return patientList;
    }


    @GetMapping("/patients/export")
    public void exportExcel() {
        List<Patient> patients = getAll();
        new Storage().exportExcel(patients);
    }

    @DeleteMapping("/patient/delete/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable("id") Long id) {
        Patient deletePatient = patientService.deletePatient(id);
        return new ResponseEntity<>(deletePatient, HttpStatus.OK);
    }

    @GetMapping("/export/{id}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable("id") Long id) throws IOException {
        Patient p = findById(id);
        String fileName = new Storage().exportWord(p);
        File file = new File(Constant.REPORT_PATH + fileName);
        HttpHeaders header = new HttpHeaders();
        Path path = file.toPath();
        String mimeType = Files.probeContentType(path);
        System.out.println(mimeType);
        header.setContentType(MediaType.valueOf(mimeType));
        byte[] data = Files.readAllBytes(path);
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(data, header, HttpStatus.OK);
    }


    @GetMapping("/hospitals")
    public List<Hospital> getHosAll() {
        return hospitalRepo.findAll();
    }


    @RequestMapping("/patients")
    public List<Patient> search(@Param("name") String name, @Param("status") String status) {
        List<Patient> patients;
        if (Valid.isNullOrEmpty(name) && Valid.isNullOrEmpty(status)) {
            patients = patientService.findAllActive();
        } else if (!Valid.isNullOrEmpty(name) && Valid.isNullOrEmpty(status)) {
            patients = patientService.searchWithKeyWord(name);
        } else if (Valid.isNullOrEmpty(name) && !Valid.isNullOrEmpty(status)) {
            patients = patientService.searchWithStatus(Integer.valueOf(status.trim()));
        } else {
            patients = patientService.searchWithStatusAndKeyWord(name, Integer.valueOf(status.trim()));
        }
        return patients;
    }

    @PostMapping("/schedule/add")
    public Schedule addContact(@RequestBody ScheduleDTO schedule) {
        Schedule sc = new Schedule();
        sc.setAddress(schedule.getAddress());
        sc.setDate(schedule.getDate());
        sc.setPeople(schedule.getPersonList());
        Schedule scheduleAdd = scheduleService.save(sc);
        return scheduleAdd;
    }

    @PostMapping("/schedule/update")
    public Schedule addPerson(@RequestBody ScheduleDTO scheduleDTO) {
        Long id = scheduleDTO.getId();
        List<Person> people = scheduleDTO.getPersonList();
        if (id != null || id != 0L) {
            Schedule sc = scheduleService.findById(id);
            if (sc != null) {
                List<Person> personList = sc.getPeople();
                personList.addAll(people);
                scheduleService.save(sc);
                return sc;
            }
        }
        return null;
    }

    @PostMapping("/patient/add")
    public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) {
        String image = patient.getImage().trim();

        Map<String, String> map = new HashMap<>();
        Storage storage = new Storage();
        Map<String, String> mapImg = storage.storeFile(image, patient.getName());
        mapImg.entrySet().stream().map((entry) -> {
            String key = entry.getKey();
            String value = entry.getValue();
            map.put(key, value);
            return key;
        }).forEachOrdered((key) -> {
            patient.setImage(key);
        });
        return ResponseEntity.ok(patientService.save(patient));
    }

    @PostMapping("/patient/update/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable Long id, @RequestBody Patient newPatient) {
        String image = newPatient.getImage().trim();

        Map<String, String> map = new HashMap<>();
        Storage storage = new Storage();
        Map<String, String> mapImg = storage.storeFile(image, newPatient.getName());
        mapImg.entrySet().stream().map((entry) -> {
            String key = entry.getKey();
            String value = entry.getValue();
            map.put(key, value);
            return key;
        }).forEachOrdered((key) -> {
            newPatient.setImage(key);
        });
        Patient patientToUpdate = patientService.findById(id);
        patientToUpdate.setName(newPatient.getName());
        patientToUpdate.setGender(newPatient.getGender());
        patientToUpdate.setDateOfBirth(newPatient.getDateOfBirth());
        patientToUpdate.setIdentityCard(newPatient.getIdentityCard());
        patientToUpdate.setPhone(newPatient.getPhone());
        patientToUpdate.setImage(newPatient.getImage());
//            Update patient address
        Address patientAddress = patientToUpdate.getAddress();
        patientAddress.setProvince(newPatient.getAddress().getProvince());
        patientAddress.setDistrict(newPatient.getAddress().getDistrict());
        patientAddress.setWard(newPatient.getAddress().getWard());
        patientAddress.setDetail(newPatient.getAddress().getDetail());
        return ResponseEntity.ok(patientService.save(patientToUpdate));
    }
}


