package com.ificovid19server.repos;

import com.ificovid19server.models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ScheduleRepo extends JpaRepository<Schedule,Long> {
}
