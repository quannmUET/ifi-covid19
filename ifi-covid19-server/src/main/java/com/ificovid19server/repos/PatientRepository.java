package com.ificovid19server.repos;

import com.ificovid19server.models.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient,Long> {
    @Query("SELECT p FROM Patient p where p.isDeleted = false and p.status =:status")
    List<Patient> findAllByStatus(@Param("status") int status);

    @Query("SELECT p FROM Patient p where p.isDeleted = false and p.status =:status and p.name LIKE CONCAT('%',:name,'%')")
    List<Patient> findAllByStatusAndKeyword(@Param("status") int status, @Param("name") String name);

    @Query("SELECT p FROM Patient p where p.isDeleted = false")
    List<Patient> findAllActive();

    @Query("SELECT p FROM Patient p where p.name LIKE CONCAT('%',:name,'%')")
    List<Patient> findAllByKeyword(@Param("name") String name);
}
