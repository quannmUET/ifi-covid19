package com.ificovid19server.repos;

import com.ificovid19server.models.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepo extends JpaRepository<Hospital,Long> {
}
