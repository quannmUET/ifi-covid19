import React from "react";
import AddressSelector from "./AddressSelector";
import { Button, Row, Col, Form } from "react-bootstrap";

class PatientInfoForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			validated: false,
			isValidForm: false,
			name: this.props.patient ? this.props.patient.name : "",
			dateOfBirth: this.props.patient ? new Date(this.props.patient.dateOfBirth).toJSON().split("T")[0] : "",
			gender: this.props.patient ? this.props.patient.gender : "",
			identityCard: this.props.patient ? this.props.patient.identityCard : "",
			phone: this.props.patient ? this.props.patient.phone : "",
			image: this.props.patient ? this.props.patient.image : "",
			status: this.props.patient ? this.props.patient.status : "",
			province: this.props.patient ? this.props.patient.address.province : "",
			district: this.props.patient ? this.props.patient.address.district : "",
			ward: this.props.patient ? this.props.patient.address.ward : "",
			detail: this.props.patient ? this.props.patient.address.detail : "",
		};
	}
	handleProvinceSelect = (province) => {
		this.setState({ province: province });
		console.log(province);
	};
	handleDistrictSelect = (district) => {
		this.setState({ district: district });
		console.log(district);
	};
	handleWardSelect = (ward) => {
		this.setState({ ward: ward });
		console.log(ward);
	};
	handleAddressDetailInput = (detail) => {
		this.setState({ detail: detail });
		console.log(detail);
	};
	imageToBase64 = (file, callback) => {
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function () {
			callback(reader.result);
		};
		reader.onerror = function (error) {
			console.log("Error: ", error);
		};
	};

	getImage = (event) => {
		console.log(event.target.parentNode);
		let image = event.target.files[0];
		this.imageToBase64(image, (result) => {
			this.setState({ image: result });
		});
	};
	handleSubmit = (event) => {
		let patient = {
			name: this.state.name ? this.state.name : this.props.patient.name,
			dateOfBirth: this.state.dateOfBirth ? this.state.dateOfBirth : this.props.patient.dateOfBirth,
			gender: this.state.gender ? this.state.gender : this.props.patient.gender,
			identityCard: this.state.identityCard ? this.state.identityCard : this.props.patient.identityCard,
			phone: this.state.phone ? this.state.phone : this.props.patient.phone,
			image: this.state.image ? this.state.image : this.props.patient.image,
			address: {
				province: this.state.province ? this.state.province : this.props.patient.address.province,
				district: this.state.district ? this.state.district : this.props.patient.address.district,
				ward: this.state.ward ? this.state.ward : this.props.patient.address.ward,
				detail: this.state.detail ? this.state.detail : this.props.patient.address.detail,
			},
		};
		let url = this.props.patient ? "patient/update/" + this.props.patient.id : "/patient/add";
		const requestOptions = {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(patient),
		};
		console.log(patient);
		console.log(requestOptions);
		fetch(url, requestOptions)
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
			})
			.catch((error) => {
				console.log("error", error);
				// alert("Error: " + error);
			});
	};
	render() {
		return (
			<>
				<Form validated={this.state.validated} onSubmit={this.handleSubmit}>
					<Row>
						<Col xs={4}>
							<fieldset>
								<legend>Patient Image:</legend>
								{/* Patient image */}
								<Form.Group as={Row} controlId="image" className="mb-1">
									<Form.Control
										type="file"
										size="sm"
										accept="*.jpg, *.png, *.gif"
										onChange={(event) => {
											this.getImage(event);
										}}
									/>
									{this.props.patient ? <img src={this.state.image} alt="Patient" /> : this.state.image ? <img src={this.state.image} alt="Patient" /> : null}
								</Form.Group>
							</fieldset>
						</Col>
						<Col xs={8}>
							{/* Personal Info */}
							<fieldset>
								<legend>Personal Information</legend>
								{/* Patient Name */}
								<Form.Group as={Row} controlId="patientName" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										Patient Name:
									</Form.Label>
									<Col xs={8}>
										<Form.Control
											type="text"
											size="sm"
											defaultValue={this.state.name}
											onBlur={(event) => {
												this.setState({ name: event.target.value });
											}}
										/>
									</Col>
								</Form.Group>
								{/* Patient Gender */}
								<Form.Group as={Row} controlId="patientGender" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										Gender:
									</Form.Label>
									<Col xs={8}>
										<Form.Select
											size="sm"
											defaultValue={this.state.gender}
											onBlur={(event) => {
												this.setState({ gender: event.target.value });
											}}
										>
											<option value="">Select Gender</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
											<option value="Other">Other</option>
										</Form.Select>
									</Col>
								</Form.Group>
								{/* Patient Birthday */}
								<Form.Group as={Row} controlId="dateOfBirth" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										Date Of Birth:
									</Form.Label>
									<Col xs={8}>
										<Form.Control
											type="date"
											size="sm"
											defaultValue={this.state.dateOfBirth}
											max={new Date().toJSON().split("T")[0]}
											onBlur={(event) => {
												this.setState({ dateOfBirth: event.target.value });
											}}
										/>
									</Col>
								</Form.Group>
								{/* Patient ID Card */}
								<Form.Group as={Row} controlId="identityCard" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										ID Card:
									</Form.Label>
									<Col xs={8}>
										<Form.Control
											type="text"
											size="sm"
											defaultValue={this.state.identityCard}
											onBlur={(event) => {
												this.setState({ identityCard: event.target.value });
											}}
										/>
									</Col>
								</Form.Group>
								{/* Patient Phone */}
								<Form.Group as={Row} controlId="phone" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										Phone Number:
									</Form.Label>
									<Col xs={8}>
										<Form.Control
											type="text"
											size="sm"
											defaultValue={this.state.phone}
											onBlur={(event) => {
												this.setState({ phone: event.target.value });
											}}
										/>
									</Col>
								</Form.Group>
								{/* Patient Address */}
								<AddressSelector
									patient={this.props.patient}
									onProvinceChange={this.handleProvinceSelect}
									onDistrictChange={this.handleDistrictSelect}
									onWardChange={this.handleWardSelect}
									onDetailChange={this.handleAddressDetailInput}
								/>
								{/* Patient status */}
								<Form.Group as={Row} controlId="status" className="mb-1">
									<Form.Label size="sm" column xs={4}>
										Patient Status:
									</Form.Label>
									<Col xs={8}>
										<Form.Select
											size="sm"
											defaultValue={this.state.status}
											onBlur={(event) => {
												this.setState({ status: event.target.value });
											}}
										>
											<option value="0">Just detected</option>
											<option value="1">Being treated</option>
											<option value="2">Died</option>
											<option value="3">Recovered</option>
										</Form.Select>
									</Col>
								</Form.Group>
							</fieldset>
							<Button variant="success" type="submit">
								{this.props.patient ? "Update" : "Add"}
							</Button>
						</Col>
					</Row>
				</Form>
			</>
		);
	}
}
export default PatientInfoForm;
