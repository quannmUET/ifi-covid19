import React from "react";
import { Button, Modal, Row, Col, Form, ListGroup, Table } from "react-bootstrap";
import fileDownload from "js-file-download";

class ViewPatientDetailModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
		};
	}

	handleClose = () => {
		this.setState({ show: false });
	};
	handleShow = () => {
		this.setState({ show: true });
	};

	renderTravelSchedule = () => {
		return this.props.patient.scheduleList.map((schedule, index) => {
			return (
				<tr key={schedule.id}>
					<td>{schedule.date}</td>
					<td>{schedule.address.detail + ", " + schedule.address.ward + ", " + schedule.address.district + ", " + schedule.address.province}</td>
					<td>
						<ListGroup as="ol" numbered>
							{schedule.people.map((person, index) => {
								let personDetail =
									"Name: " +
									person.name +
									"\n" +
									"Date Of Birth: " +
									person.dateOfBirth +
									"\n" +
									"Gender: " +
									person.gender +
									"\n" +
									"ID Card: " +
									person.identityCard +
									"\n" +
									"Phone Number: " +
									person.phone +
									"\n" +
									"Address: " +
									person.address.detail +
									", " +
									person.address.ward +
									", " +
									person.address.district +
									", " +
									person.address.province;
								return (
									<ListGroup.Item as="li" key={person.id} title={personDetail}>
										{person.name + " (" + person.identityCard + ")"}
									</ListGroup.Item>
								);
							})}
						</ListGroup>
					</td>
				</tr>
			);
		});
	};

	exportDocx = () => {
		fetch("/patient/export/" + this.props.patient.id)
			.then((response) => {
				return response.blob();
			})
			.then((blob) => {
				const fileName = "Patient-" + this.props.patient.id + "-" + this.props.patient.name + ".docx";
				fileDownload(blob, fileName);
			});
	};

	render() {
		return (
			<>
				<Button variant="info" size="sm" className="me-1" title="View this patient's details" onClick={this.handleShow}>
					<i className="fas fa-eye fa-sm fa-fw"></i>
				</Button>

				<Modal show={this.state.show} onHide={this.handleClose} animation={false} size="xl">
					<Modal.Header closeButton>
						<Button variant="info" onClick={this.exportDocx}>
							Export Info To .docx
						</Button>
					</Modal.Header>
					<Modal.Body>
						<Row>
							<Col xs={3}>
								<img src={this.props.patient.image} alt={"Patient " + this.props.patient.id} />
							</Col>
							<Col xs={9}>
								{/* Personal Information */}
								<fieldset disabled>
									<legend>Personal Information</legend>
									{/* Patient ID */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Patient ID:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.id} />
										</Col>
									</Form.Group>
									{/* Patient Name */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Patient Name:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.name} />
										</Col>
									</Form.Group>
									{/* Patient Gender */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Gender:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.gender} />
										</Col>
									</Form.Group>
									{/* Patient Birthday */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Date Of Birth:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.dateOfBirth} />
										</Col>
									</Form.Group>
									{/* Patient ID Card */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											ID Card:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.identityCard} />
										</Col>
									</Form.Group>
									{/* Patient Address */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Address:
										</Form.Label>
										<Col xs={8}>
											<Form.Control
												readOnly
												type="text"
												size="sm"
												defaultValue={
													this.props.patient.address.detail +
													", " +
													this.props.patient.address.ward +
													", " +
													this.props.patient.address.district +
													", " +
													this.props.patient.address.province
												}
											/>
										</Col>
									</Form.Group>
									{/* Patient Phone */}
									<Form.Group as={Row} controlId="form-group-id" className="mb-1">
										<Form.Label size="sm" column xs={4}>
											Phone Number:
										</Form.Label>
										<Col xs={8}>
											<Form.Control readOnly type="text" size="sm" defaultValue={this.props.patient.phone} />
										</Col>
									</Form.Group>
								</fieldset>
								{/* Travel Schedule */}
								<fieldset disabled="disabled">
									<legend>Travel Schedule</legend>
									<Table striped bordered responsive>
										<thead>
											<tr>
												<th>Time</th>
												<th>Address</th>
												<th>Contact People</th>
											</tr>
										</thead>
										<tbody>{this.renderTravelSchedule()}</tbody>
									</Table>
								</fieldset>
							</Col>
						</Row>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={this.handleClose}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>
			</>
		);
	}
}
export default ViewPatientDetailModal;
