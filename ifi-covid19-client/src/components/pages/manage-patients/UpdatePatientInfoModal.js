import React from "react";
import { Button, Modal } from "react-bootstrap";
import PatientInfoForm from "../../common/PatientInfoForm";

class UpdatePatientInfoModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
		};
	}

	handleClose = () => {
		this.setState({ show: false });
	};
	handleShow = () => {
		this.setState({ show: true });
	};

	render() {
		return (
			<>
				<Button variant="warning" size="sm" className="me-1" title="Update this patient's info" onClick={this.handleShow}>
					<i className="fas fa-pencil-alt fa-sm fa-fw"></i>
				</Button>

				<Modal show={this.state.show} onHide={this.handleClose} animation={false} size="xl">
					<Modal.Header closeButton>
						<Modal.Title>Update Info: Patient {this.props.patient.name}</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<PatientInfoForm patient={this.props.patient} />
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={this.handleClose}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>
			</>
		);
	}
}
export default UpdatePatientInfoModal;
