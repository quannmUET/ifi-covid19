import React from "react";
import PatientInfoForm from "../../common/PatientInfoForm";
import { Button, Modal } from "react-bootstrap";

class AddPatientModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
		};
	}

	handleClose = () => {
		this.setState({
			show: false,
		});
	};
	handleShow = () => {
		this.setState({ show: true });
	};

	render() {
		return (
			<>
				<Button variant="success" size="sm" className="me-1" title="View this patient's details" onClick={this.handleShow}>
					<i className="fas fa-user-plus fa-sm fa-fw"></i> Add Patient
				</Button>

				<Modal show={this.state.show} onHide={this.handleClose} animation={false} size="xl">
					<Modal.Header closeButton>
						<Modal.Title>Add New Patient</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<PatientInfoForm />
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={this.handleClose}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>
			</>
		);
	}
}
export default AddPatientModal;
