import React from "react";
import { Table } from "react-bootstrap";
import AddPatientModal from "./AddPatientModal";
import DeleteConfirmationModal from "./DeleteConfirmationModal";
import UpdatePatientInfoModal from "./UpdatePatientInfoModal";
import ViewPatientDetailModal from "./ViewPatientDetailModal";

class PatientTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			patients: [],
		};
	}
	componentDidMount() {
		this.getPatientData();
	}
	getPatientData = () => {
		fetch("/patient/all")
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
				this.setState({ patients: data });
			});
	};

	renderTableData() {
		return this.state.patients.map((patient, index) => {
			return (
				<tr key={patient.id}>
					<td>{patient.id}</td>
					<td>
						<img src={patient.image} alt={"Patient " + patient.id} width="100px" />
					</td>
					<td>{patient.name}</td>
					<td>{patient.gender}</td>
					<td>{new Date(patient.dateOfBirth).toLocaleDateString("vi-VN")}</td>
					<td>{patient.identityCard}</td>
					<td>{patient.address.detail + ", " + patient.address.ward + ", " + patient.address.district + ", " + patient.address.province}</td>
					<td>
						<ViewPatientDetailModal patient={patient} />
						<UpdatePatientInfoModal patient={patient} onUpdatePatient={this.getPatientData} />
						<DeleteConfirmationModal patient={patient} onDeletePatient={this.getPatientData} />
					</td>
				</tr>
			);
		});
	}

	render() {
		return (
			<>
				<div className="my-3">
					<AddPatientModal onAddPatient={this.reloadTable} />
				</div>
				<Table striped responsive id="patientTable">
					<thead>
						<tr>
							<th>ID</th>
							<th>Image</th>
							<th>Name</th>
							<th>Gender</th>
							<th>Birthday</th>
							<th>ID Card</th>
							<th>Address</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>{this.renderTableData()}</tbody>
				</Table>
			</>
		);
	}
}
export default PatientTable;
