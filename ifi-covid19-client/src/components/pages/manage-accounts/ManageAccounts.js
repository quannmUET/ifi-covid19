import React from "react";
import Header from "../../common/Header";
import { Container } from "react-bootstrap";
import AccountTable from "./AccountTable";

class ManageAccounts extends React.Component {
	render() {
		return (
			<Container>
				<Header username="admin" />
				<AccountTable />
			</Container>
		);
	}
}
export default ManageAccounts;
