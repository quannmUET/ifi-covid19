import React from "react";
import { Table, Button } from "react-bootstrap";
import CreateNewAccountModal from "./CreateNewAccountModal";

class AccountTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			accounts: [],
		};
	}

	componentDidMount() {
		fetch("/account/all")
			.then((response) => response.json())
			.then((data) => {
				this.setState({ accounts: data });
			});
	}

	reloadTable = () => {
		fetch("/account/all")
			.then((response) => response.json())
			.then((data) => {
				this.setState({ accounts: data });
			});
	};

	deleteAccount(id) {
		fetch("/account/" + id, {
			method: "DELETE",
		})
			.then((response) => response.json())
			.then((data) => {
				this.reloadTable();
			});
	}

	promoteAccount(id, newAccount) {
		fetch("/account/" + id, {
			method: "PUT",
			body: JSON.stringify({
				account: newAccount,
			}),
			headers: {
				"Content-Type": "application/json",
			},
		})
			.then((response) => response.json())
			.then((data) => {
				this.reloadTable();
			});
	}

	renderTableData() {
		return this.state.accounts.map((account, index) => {
			return (
				<tr key={account.id}>
					<td>{account.id}</td>
					<td>{account.username}</td>
					<td>{account.fullName}</td>
					<td>{account.admin + ""}</td>
					<td>
						<Button
							variant="warning"
							className="me-1"
							size="sm"
							title="Promote this account"
							onClick={() => {
								account.admin = true;
								this.promoteAccount(account.id, account);
							}}
						>
							<i className="fas fa-user-cog fa-sm fa-fw"></i>
						</Button>
						<Button variant="danger" className="me-1" size="sm" title="Delete this account" onClick={() => this.deleteAccount(account.id)}>
							<i className="fas fa-trash fa-sm fa-fw"></i>
						</Button>
					</td>
				</tr>
			);
		});
	}

	render() {
		return (
			<>
				<div className="my-3">
					<CreateNewAccountModal onAddAccount={this.reloadTable} />
				</div>
				<Table striped responsive>
					<thead>
						<tr>
							<th>Account ID</th>
							<th>Username</th>
							<th>Fullname</th>
							<th>Admin</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>{this.renderTableData()}</tbody>
				</Table>
			</>
		);
	}
}
export default AccountTable;
