import React from "react";
import { Button, Modal, Form, Col, Row } from "react-bootstrap";

class CreateNewAccountModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			username: "",
			password: "",
			confirmPassword: "",
			fullName: "",
			isAdmin: false,
			validated: false,
			errors: { username: "", password: "", confirmPassword: "", fullName: "" },
		};
	}

	handleClose = () => {
		this.setState({ show: false });
	};
	handleShow = () => {
		this.setState({ show: true });
	};

	handleSubmit = (event) => {
		let form = event.target;
		console.log(form.checkValidity());
		debugger;
		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		} else {
			let account = { username: this.state.username, password: this.state.password, fullName: this.state.fullName, isAdmin: this.state.isAdmin };
			const requestOptions = {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(account),
			};
			fetch("/account/new", requestOptions)
				.then((response) => response.json())
				.then((data) => {
					console.log(data);
					this.setState({ show: false });
					this.props.onAddAccount();
				})
				.catch((error) => {
					console.log(error);
					alert(error);
				});
		}
	};

	render() {
		return (
			<>
				<Button variant="success" size="sm" title="Create new account" onClick={this.handleShow}>
					<i className="fas fa-user-plus fa-sm fa-fw"></i> Add Account
				</Button>

				<Modal show={this.state.show} onHide={this.handleClose} animation={false} size="lg">
					<Modal.Header closeButton>
						<Modal.Title>Create New Account</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form onSubmit={this.handleSubmit} validated={this.state.validated}>
							<Form.Group as={Row} controlId="username" className="mb-3">
								<Form.Label column xs={4}>
									Username:
								</Form.Label>
								<Col xs={8}>
									<Form.Control
										type="text"
										placeholder="Enter username"
										name="username"
										onBlur={(event) => {
											this.setState({ username: event.target.value });
										}}
									/>
									<Form.Control.Feedback>Looks good!</Form.Control.Feedback>
									<Form.Control.Feedback type="invalid">{this.state.errors.username}</Form.Control.Feedback>
								</Col>
							</Form.Group>
							<Form.Group as={Row} controlId="password" className="mb-3">
								<Form.Label column xs={4}>
									Password:
								</Form.Label>
								<Col xs={8}>
									<Form.Control
										type="password"
										placeholder="Enter password"
										name="password"
										onBlur={(event) => {
											this.setState({ password: event.target.value });
										}}
									/>
									<Form.Control.Feedback>Looks good!</Form.Control.Feedback>
									<Form.Control.Feedback type="invalid">{this.state.errors.password}</Form.Control.Feedback>
								</Col>
							</Form.Group>
							<Form.Group as={Row} controlId="confirmPassword" className="mb-3">
								<Form.Label column xs={4}>
									Confirm password:
								</Form.Label>
								<Col xs={8}>
									<Form.Control
										type="password"
										placeholder="Enter password again"
										name="confirmPassword"
										onBlur={(event) => {
											this.setState({ confirmPassword: event.target.value });
										}}
									/>
									<Form.Control.Feedback>Looks good!</Form.Control.Feedback>
									<Form.Control.Feedback type="invalid">{this.state.errors.confirmPassword}</Form.Control.Feedback>
								</Col>
							</Form.Group>
							<Form.Group as={Row} controlId="fullName" className="mb-3">
								<Form.Label column xs={4}>
									Full Name:
								</Form.Label>
								<Col xs={8}>
									<Form.Control
										type="text"
										placeholder="Enter full name"
										name="fullName"
										onBlur={(event) => {
											this.setState({ fullName: event.target.value });
										}}
									/>
									<Form.Control.Feedback>Looks good!</Form.Control.Feedback>
									<Form.Control.Feedback type="invalid">{this.state.errors.fullName}</Form.Control.Feedback>
								</Col>
							</Form.Group>
							<Form.Group as={Row} controlId="isAdmin" className="mb-3">
								<Form.Label column xs={4}>
									Is ADMINISTRATOR?
								</Form.Label>
								<Col xs={8}>
									<Form.Check
										type="checkbox"
										label="ADMINISTRATOR Account"
										name="isAdmin"
										onChange={(event) => {
											this.setState({ isAdmin: event.target.checked });
										}}
									/>
								</Col>
							</Form.Group>
							<Button variant="primary" type="submit" className="me-1" disabled={!this.state.validated}>
								Create Account
							</Button>
							<Button variant="secondary" onClick={this.handleClose}>
								Close
							</Button>
						</Form>
					</Modal.Body>
				</Modal>
			</>
		);
	}
}
export default CreateNewAccountModal;
