import "./App.css";
import React from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import ManagePatients from "./components/pages/manage-patients/ManagePatients";
import ManageAccounts from "./components/pages/manage-accounts/ManageAccounts";
import LoginPage from "./components/pages/login/LoginPage";
import Homepage from "./components/pages/homepage/Homepage";

function App() {
	return (
		<BrowserRouter>
			<Routes>
				<Route exact path="/manage-patients" element={<ManagePatients />} />
				<Route exact path="/manage-accounts" element={<ManageAccounts />} />
				<Route exact path="/login" element={<LoginPage />} />
				<Route exact path="/" element={<Homepage />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
